package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	rd "math/rand"
	"net"
	"os"
	"time"
)

type CertificateFields struct {
	Name             string
	Locality         string
	Country          string
	Province         string
	Organization     string
	OrganizationUnit string
	Email            string
	DomainName       string
}

func main() {
	fields := FillCertificateFields()

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}

	rd.Seed(time.Now().UnixNano())
	serialNumber := int64(rd.Intn(999999999))

	firstCert := x509.Certificate{
		SerialNumber: big.NewInt(serialNumber),
		Subject: pkix.Name{
			CommonName:         "main.com",
			Organization:       []string{fields.Organization},
			OrganizationalUnit: []string{fields.OrganizationUnit},
			Locality:           []string{fields.Locality},
			Province:           []string{fields.Province},
			Country:            []string{fields.Country}},
		EmailAddresses:        []string{fields.Email},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(365 * 24 * time.Hour),
		BasicConstraintsValid: true,
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		DNSNames:              []string{"main.com"},
		IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
	}

	CreateCertificate("mainCert.crt", "mainKey.crt", firstCert, firstCert, privateKey.PublicKey, privateKey)

	rd.Seed(time.Now().UnixNano())
	serialNumber = int64(rd.Intn(999999999))

	secondCert := x509.Certificate{
		SerialNumber: big.NewInt(serialNumber),
		Subject: pkix.Name{
			CommonName:         "intermdeiate.com",
			Organization:       []string{fields.Organization},
			OrganizationalUnit: []string{fields.OrganizationUnit},
			Locality:           []string{fields.Locality},
			Province:           []string{fields.Province},
			Country:            []string{fields.Country}},
		EmailAddresses:        []string{fields.Email},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(365 * 24 * time.Hour),
		BasicConstraintsValid: true,
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		DNSNames:              []string{"intermdeiate.com"},
		IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
	}

	CreateCertificate("intermediateCert.crt", "intermediateKey.crt", secondCert, firstCert, privateKey.PublicKey, privateKey)

	rd.Seed(time.Now().UnixNano())
	serialNumber = int64(rd.Intn(999999999))

	thirdCert := x509.Certificate{
		SerialNumber: big.NewInt(serialNumber),
		Subject: pkix.Name{
			CommonName:         "myserverfit.com",
			Organization:       []string{fields.Organization},
			OrganizationalUnit: []string{fields.OrganizationUnit},
			Locality:           []string{fields.Locality},
			Province:           []string{fields.Province},
			Country:            []string{fields.Country}},
		EmailAddresses:        []string{fields.Email},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(365 * 24 * time.Hour),
		BasicConstraintsValid: true,
		IsCA:                  true,
		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		DNSNames:              []string{"myserferfit.com"},
		IPAddresses:           []net.IP{net.ParseIP("127.0.0.1")},
	}

	CreateCertificate("serverCert.crt", "serverKey.crt", thirdCert, secondCert, privateKey.PublicKey, privateKey)

}

func CreateCertificate(certName, keyCert string, childCert, parentCert x509.Certificate, pubKey rsa.PublicKey, privKey *rsa.PrivateKey) {
	derBytes, err := x509.CreateCertificate(rand.Reader, &childCert, &parentCert, &pubKey, privKey)
	if err != nil {
		panic(err)
	}

	certFile, err := os.Create(certName)
	if err != nil {
		panic(err)
	}

	defer certFile.Close()
	pem.Encode(certFile, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	keyFile, err := os.Create(keyCert)
	if err != nil {
		panic(err)
	}
	defer keyFile.Close()
	pem.Encode(keyFile, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
}

func FillCertificateFields() *CertificateFields {
	var fields CertificateFields

	fmt.Print("Name: ")
	fmt.Scan(&fields.Name)

	fmt.Print("Locality: ")
	fmt.Scan(&fields.Locality)

	fmt.Print("Country: ")
	fmt.Scan(&fields.Country)

	fmt.Print("Province: ")
	fmt.Scan(&fields.Province)

	fmt.Print("Organization: ")
	fmt.Scan(&fields.Organization)

	fmt.Print("Organization Unit: ")
	fmt.Scan(&fields.OrganizationUnit)

	fmt.Print("Email: ")
	fmt.Scan(&fields.Email)

	fmt.Print("Domain Name: ")
	fmt.Scan(&fields.DomainName)

	return &fields
}
