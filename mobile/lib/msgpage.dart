import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'dart:ffi';
import 'package:ffi/ffi.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:path_provider/path_provider.dart';

typedef CalculateHashC = Void Function(Pointer<Uint8>, IntPtr, Pointer<Uint8>);
typedef CalculateHashDart = void Function(Pointer<Uint8>, int, Pointer<Uint8>);

class MyResponse {
  final String message;

  MyResponse({required this.message});

  factory MyResponse.fromJson(Map<String, dynamic> json) {
    return MyResponse(
      message: json['message'],
    );
  }
}

class MyPage extends StatefulWidget {
  @override
  _MyPage createState() => _MyPage();
}

typedef NativeFunc = Int32 Function();
typedef DartFunc = int Function();

class _MyPage extends State<MyPage> {
  final TextEditingController messageController = TextEditingController();

  Timer? timer;
  List<double> accelerometerData = [];
  List<double> gyroscopeData = [];
  final metricsMessageController = TextEditingController();
  int xorResult = 0;



  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  Future<void> sendMessage() async {
    final String message = messageController.text;

    final httpContext = SecurityContext.defaultContext;
    final chainBytes = await rootBundle.load('assets/chain.crt');
    final keyBytes = await rootBundle.load('assets/serverKey.crt');
    httpContext.useCertificateChainBytes(chainBytes.buffer.asUint8List());
    httpContext.usePrivateKeyBytes(keyBytes.buffer.asUint8List());
    final httpClient = HttpClient(context: httpContext);
    httpClient.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final client = IOClient(httpClient);

    try {
      var dylib = DynamicLibrary.open("/home/ghotsfed/HomeTask/mobile/jni/libhash-lib.so"); // Путь к вашей нативной библиотеке
      final calculateHash = dylib.lookupFunction<CalculateHashC, CalculateHashDart>('calculateHash');

      //final message = 'Hello, World!'; // Ваше сообщение для хэширования
      final messageUtf8 = message.toNativeUtf8();
      final messagePointer = messageUtf8.cast<Uint8>();
      final messageLength = message.length;
      final hashBuffer = calloc<Uint8>(32);

      calculateHash(messagePointer, messageLength, hashBuffer);

      final hash = hashBuffer.asTypedList(32);

      print('Hash: $hash');

      String strHash = "";
      for (int i = 0; i < hash.length; i++) {
        print(hash[i]);
        strHash += hash[i].toString();
      }

      calloc.free(messagePointer);
      calloc.free(hashBuffer);

      Map<String, dynamic> data = {
        'message': message,
        'hash': strHash,
      };

      print(jsonEncode(data));

      final response = await client.post(
        Uri.parse('https://myserverfit.com/hash-message'),
        body: jsonEncode(data),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        final myResponse = MyResponse.fromJson(responseData);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Получено"),
              content: Text(myResponse.message),
              actions: <Widget>[
                ElevatedButton(
                  child: Text("Закрыть"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else {
        final responseData = jsonDecode(response.body);
        final myResponse = MyResponse.fromJson(responseData);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Ошибка"),
              content: Text(myResponse.message),
              actions: <Widget>[
                ElevatedButton(
                  child: Text("Закрыть"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ошибка"),
            content: Text(e.toString()),
            actions: <Widget>[
              ElevatedButton(
                child: Text("Закрыть"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }


  Future<void> GetInfo() async {
    timer = Timer.periodic(const Duration(seconds: 3), (Timer t) {
      setState(() {
        if (accelerometerData.length >= 3 && gyroscopeData.length >= 3) {
          int accelerometerBits = (accelerometerData.last * 255).toInt() & 0xFF;
          int gyroscopeBits = (gyroscopeData.last * 255).toInt() & 0xFF;
          xorResult = accelerometerBits ^ gyroscopeBits;
        }
      });
    });

    accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        accelerometerData = [event.x, event.y, event.z];
      });
    });

    gyroscopeEvents.listen((GyroscopeEvent event) {
      setState(() {
        gyroscopeData = [event.x, event.y, event.z];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Отправка сообщения')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: messageController,
              decoration: InputDecoration(labelText: 'Введите сообщение'),
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: sendMessage,
              child: Text('Отправить'),
            ),
            ElevatedButton(
              onPressed: GetInfo,
              child: Text('Получить данные с акселерометра'),
            ),
            SizedBox(height: 16.0),
            Text('Акселерометр:'),
            Text('x=${accelerometerData.isNotEmpty ? accelerometerData[0].toStringAsFixed(3) : 0.000}'),
            Text('y=${accelerometerData.isNotEmpty ? accelerometerData[1].toStringAsFixed(3) : 0.000}'),
            Text('z=${accelerometerData.isNotEmpty ? accelerometerData[2].toStringAsFixed(3) : 0.000}'),
            SizedBox(height: 16.0),
            Text('Гироскоп:'),
            Text('x=${gyroscopeData.isNotEmpty ? gyroscopeData[0].toStringAsFixed(3) : 0.000}'),
            Text('y=${gyroscopeData.isNotEmpty ? gyroscopeData[1].toStringAsFixed(3) : 0.000}'),
            Text('z=${gyroscopeData.isNotEmpty ? gyroscopeData[2].toStringAsFixed(3) : 0.000}'),
            SizedBox(height: 16.0),
            Text('XOR результат: $xorResult'),
          ],
        ),
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: MyPage(),
  ));
}
