$TTL 86400
@   IN   SOA   ns1.myserverfit.com. admin.myserverfit.com. (
            2023051601   ; Serial
            3600         ; Refresh
            1800         ; Retry
            604800       ; Expire
            86400        ; Minimum TTL
)

@   IN   NS    ns1.myserverfit.com.
@   IN   A     172.17.0.1
