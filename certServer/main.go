package main

import (
	"crypto/sha256"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type Response struct {
	Message string `json:"message"`
}

type Request struct {
	Message string `json:"message"`
	Hash    string `json:"hash"`
}

func main() {

	hostIP := os.Getenv("DOCKER_HOST_IP")
	port := ":8085"

	chain, err := tls.LoadX509KeyPair("chain.crt", "serverKey.crt")
	if err != nil {
		log.Fatal(err)
	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{chain},
		MinVersion:   tls.VersionTLS12,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		},
	}

	server := &http.Server{
		Addr:      hostIP + port,
		TLSConfig: tlsConfig,
	}

	http.HandleFunc("/index", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, client! Server is working")
	})

	http.HandleFunc("/hash-message", func(w http.ResponseWriter, r *http.Request) {
		var req Request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			fmt.Println(err)
			http.Error(w, "Failed to parse JSON request", http.StatusBadRequest)
			return
		}

		fmt.Println(req.Message)
		fmt.Println(req.Hash)

		checkHash := calculateHash([]byte(req.Message))

		if req.Hash != checkHash {
			http.Error(w, "Hash not similar!", http.StatusInternalServerError)
			return
		}

		response := Response{
			Message: "Hello, client! The hash of your message is correct. Thank You!",
		}

		jsonResponse, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Failed to marshal JSON response", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		w.Write(jsonResponse)
	})

	log.Printf("Server working on port %s\n", port)

	if err := server.ListenAndServeTLS("", ""); err != nil {
		panic(err)
	}
}

func calculateHash(message []byte) string {
	hash := sha256.Sum256(message)
	var strHash string
	for _, v := range hash {
		strHash += fmt.Sprintf("%d", v)
	}

	return strHash
}
